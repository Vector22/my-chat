from flask import Flask
from flask_socketio import SocketIO
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager

from .. import config

socketio = SocketIO()
db = SQLAlchemy()


def create_app():
    """Create an application."""

    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object(config.ConfigDev)

    # Login Manager
    login = LoginManager()
    # default login view
    login.login_view = 'auth.login'

    # Initialize Plugins
    login.init_app(app)
    db.init_app(app)

    # DB migration
    Migrate(app, db)

    # Import the models
    from . import models

    #  User loader function, that can be called to load a user given the ID
    @login.user_loader
    def load_user(id):
        return models.User.query.get(int(id))


    # Use blueprint to organize the project in
    # differents apps
    from .main import main as main_blueprint
    from .auth import auth as auth_blueprint
    app.register_blueprint(main_blueprint)
    app.register_blueprint(auth_blueprint)

    socketio.init_app(app)
    return app

