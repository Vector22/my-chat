const domElements = {
    // chatBox: document.getElementById('chat'),

    leaveLink: document.querySelector('.leave'),
}
const textBox = document.getElementById('text');
const speechBox = document.getElementById('speech-box');


// socket connection for a specific room
let socket = io.connect('http://'+ document.domain +':' + location.port + '/chat');


socket.on('connect', () => {
    // Emit the 'joined event to the server'
    socket.emit('joined', {});
});

// Action to perform when a user join/leave a channel
socket.on('status', (data) => {
    console.log(data.msg);
    // domElements.chatBox.value += '<' + data.msg + '>\n';
    // domElements.chatBox.scrollTop = domElements.chatBox.scrollHeight;
});

//
socket.on('message', (data) => {
    // domElements.chatBox.value += data.msg + '\n';
    // domElements.chatBox.scrollTop = domElements.chatBox.scrollHeight;
    const date = new Date();
    const now = date.toGMTString();
    // 1) Add the received message to the message box
    const receivedHtml = `<div class="sub-box received">${data.msg}
        <div><small>${now}</small></div></div></div>`;
    speechBox.insertAdjacentHTML("beforeend", receivedHtml);
});

// Send the message to the server and clear the message field
textBox.addEventListener('keypress', (event) => {

    let text;
    let code = event.keyCode || event.which;

    if (code == 13) {
        text = textBox.value;
        textBox.value = "";
        socket.emit('text', {msg: text});
    }
});

const leave_room = function() {
    socket.emit('left', {}, () => {
        socket.disconnect();
        //go back to the login page
        homeUrl = 'http://'+ document.domain +':' + location.port
        window.location.href = homeUrl;
    });
};

domElements.leaveLink.addEventListener('click', leave_room);
