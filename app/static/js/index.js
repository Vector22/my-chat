const usersBox = document.getElementById('users-box');
const usersBtn = document.getElementById('update-btn');

/* ==============================
    Update connected users list
=============================== */

// Update automatically the connected
// users list on page load
window.onload = function() {
    callAjax();
}

// When a user whant to update manualy the
// connected users list
usersBtn.addEventListener('click', (e) => {
    callAjax();
});

// Ajax call body function
const callAjax = () => {
    const targetUrl = '/connected_users';
    $.ajax({
        type: 'GET',
        url: targetUrl,
        success: function(data) {
            updateUsersList(JSON.parse(data));
        },
        error: function(error) {
            console.log(error);
        }
    });
};

// Update the connected users list
// take a json array of object
// (users returned by the /connected_users view)
const updateUsersList = jsondata => {
    users = jsondata['connected_users'];
    // Clear the old list
    emptyOldsUsersList();
    // Add the updated connected users list
    users.forEach(renderUser);

};

// Construct a markup for render a html user box
// take an user object {username: , email:}
const renderUser = user => {
    url = 'http://'+ document.domain +':' + location.port + '/private_chat/';
    url += user.username;
    html = `<div class="box ">
                <a href="${String(url)}">
                    <article class="media" id="${ user.username }">
                        <div class="media-left">
                          <figure class="image is-64x64">
                            <img src="https://bulma.io/images/placeholders/128x128.png" alt="Image">
                          </figure>
                        </div>
                        <div class="media-content">
                          <div class="content">
                              <strong>${ user.username }</strong>
                              <small>${ user.email }</small>
                          </div>
                        </div>
                    </article>
                </a>
            </div>
    `;

    // Insert the generated markup in the DOM
    usersBox.insertAdjacentHTML('beforeend', html);
};

// Empty the olds connected users list
const emptyOldsUsersList = () => {
        usersBox.innerHTML = "";
};
