/* ==================================
    Private message socket handler
================================== */

// const chatBox = document.getElementById('chat');
const speechBox = document.getElementById('speech-box');
const textBox = document.getElementById('text');
const leaveLink = document.querySelector('.leave');

let socket = io.connect('http://'+ document.domain +':' + location.port + '/private_chat');

socket.on('connect', () => {
    // Emit the 'join_private event to the server'
    socket.emit('join_private', {});
});

socket.on('private_connection', (data) => {

    console.log(data.msg);
    // chatBox.value += '<' + data.msg + '>\n';
    // chatBox.scrollTop = chatBox.scrollHeight;
});

// Send the message to the server and clear the message field
textBox.addEventListener('keypress', (event) => {
    let text, sendedHtml, date, now;
    let code = event.keyCode || event.which;

    if (code == 13) {
        text = textBox.value;
        date = new Date();
        now = date.toGMTString();

        // 1) Add the sended message to the message list
        sendedHtml = `<div class="sub-box sended">${text}
        <div><small>${now}</small></div></div>`;
        speechBox.insertAdjacentHTML("beforeend", sendedHtml);
        // 2) Clear the input field
        textBox.value = "";
        socket.emit('private_message', {msg: text});
    }
});


socket.on('message_private', (data) => {
    const date = new Date();
    const now = date.toGMTString();
    // 1) Add the received message to the message box
    const receivedHtml = `<div class="sub-box received">${data.msg}
        <div><small>${now}</small></div></div></div>`;
    speechBox.insertAdjacentHTML("beforeend", receivedHtml);
    // chatBox.value += data.msg + '\n';
    // chatBox.scrollTop = chatBox.scrollHeight;
});

const leave_private_chat = function() {
    socket.emit('left_private', {}, () => {
        socket.disconnect();
        //go back to the login page
        homeUrl = 'http://'+ document.domain +':' + location.port
        window.location.href = homeUrl;
    });
};

leaveLink.addEventListener('click', leave_private_chat);
