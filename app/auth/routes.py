from flask import (session, redirect, url_for, render_template,
                   request, flash)
from flask_login import login_user, logout_user, login_required, current_user
from werkzeug.security import generate_password_hash, check_password_hash

from ..models import User
from .. import db

from . import auth


@auth.route('/login', methods=['POST','GET'])
def login():
    """ Login the user """
    users = []  # flask.g users variable
    new_user = {}  # The new logged in user

    # This happens when the user try to send its credentials
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')
        remember = True if request.form.get('remember') else False

        user = User.query.filter_by(email=email).first()

        # check if user actually exists
        # take the user supplied password, hash it, and compare it to the hashed password in database
        if not user or not user.check_password(password):
            # if user doesn't exist or password is wrong, reload the page
            flash('Please check your login details and try again.')
            return redirect(url_for('auth.login'))

        # if the above check passes, then we know the user has
        # the right credentials
        login_user(user, remember=remember)
        user.is_online = True
        db.session.commit()

        return redirect(url_for('main.index'))
    # Show the login page to the user
    return render_template('auth/login.html')

@auth.route('/signup', methods=['POST','GET'])
def signup():
    """ Register a user """
    # Form is filled then we send it via POST
    if request.method == 'POST':
        username = request.form.get('username')
        email = request.form.get('email')
        password = request.form.get('password')

        # If this returns a user, then the email already exists in database
        user = User.query.filter_by(email=email).first()

        # If a user is found, redirect back to signup page so user can try again
        if user:
            flash('Email address already exists')
            return redirect(url_for('auth.signup'))

        # Create new user with the form data.
        # Hash the password so plaintext version isn't saved.
        new_user = User(email=email, username=username,
                        password_hash=generate_password_hash(password,
                                                        method='sha256')
                        )

        # add the new user to the database
        db.session.add(new_user)
        db.session.commit()

        return redirect(url_for('auth.login'))

    # Show the signup page
    return render_template('auth/signup.html')


@auth.route('/logout')
@login_required
def logout():
    """ Logout the user """
    user = User.query.filter_by(email=current_user.email).first()
    user.is_online = False
    db.session.commit()

    logout_user()
    return redirect(url_for('auth.login'))
