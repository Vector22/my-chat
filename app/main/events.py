from flask import session, request
from flask_socketio import emit, join_room, leave_room

from .. import socketio

users = {}


@socketio.on('joined', namespace='/chat')
def joined(message):
    """Sent by clients when they enter a room.
    A status message is broadcast to all people in the room."""

    room = session.get('room')
    join_room(room)
    emit('status', {'msg': session.get('username') + ' has entered the room.'}, room=room)


@socketio.on('text', namespace='/chat')
def text(message):
    """Sent by a client when the user entered a new message.
    The message is sent to all people in the room."""
    room = session.get('room')
    emit('message', {'msg': session.get('username') + ': ' + message['msg']}, room=room)


@socketio.on('left', namespace='/chat')
def left(message):
    """Sent by clients when they leave a room.
    A status message is broadcast to all people in the room."""

    # TODO: remove the user sid from the session's user variable
    room = session.get('room')
    leave_room(room)
    emit('status', {'msg': session.get('username') + ' has left the room.'}, room=room)


@socketio.on('join_private', namespace='/private_chat')
def joined(message):
    # Associate each joined user to a sessionId:request.sid
    sender = session.get('sender')
    users[sender] = request.sid

    notification = f"Send a private message to {session.get('send_to')}"

    emit('private_connection', {'msg': notification})


@socketio.on('private_message', namespace='/private_chat')
def private(message):
    sender = session.get('sender')
    send_to = session.get('send_to')


    if send_to in users:
        print(f"\n\n{sender} send a message to {send_to}: {message}\n\n")
        emit('message_private', {'msg': message['msg'], 'from': sender},
            room=users[send_to])
    else:
        emit('user_not_connected', {'from': sender})
