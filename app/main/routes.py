from flask import session, redirect, url_for, render_template, request, flash
from flask_login import current_user, login_required

from ..models import User, Channel
from .. import db
from . import main
from .forms import EnterRoomForm

import json

@main.route('/', methods=['GET', 'POST'])
@login_required
def index():
    """Login form to enter a room."""

    # statics predefined rooms
    rooms = []
    room1 = {'name': 'room-1', 'description': 'This is the Generale room'}
    room2 = {'name': 'room-2', 'description': 'This is the Second room'}
    room3 = {'name': 'room-3', 'description': 'This is the Third room'}

    customs_channels = Channel.query.filter_by(is_removable=True)
    customs_rooms = [{'name': channel.name, 'description': channel.description}\
    for channel in customs_channels]

    rooms = [room1, room2, room3]

    return render_template('index.html', rooms=rooms,
                           customs_rooms=customs_rooms)


@main.route('/chat')
@login_required
def chat():
    """Chat room. The user's name and room must be stored in
    the session."""
    name = session.get('username', '')
    room = session.get('room', '')
    if name == '' or room == '':
        return redirect(url_for('.index'))
    return render_template('chat.html', name=name, room=room)


@main.route('/join/<username>/<room>')
@login_required
def join_room(username, room):
    # Set the sessions variables
    session['username'] = username
    session['room'] = room
    # Redirect the user to the chat room
    return redirect(url_for('.chat'))


@main.route('/profile/<username>')
@login_required
def profile(username):
    return render_template('profile.html', username=username)


@main.route('/connected_users', methods=['GET'])
def connected_user():
    users_online = User.query.filter_by(is_online=True)
    users_online = users_online.filter(User.email!=current_user.email)

    users_online = [{'username': user.username, 'email': user.email}\
    for user in users_online]

    return json.dumps({'connected_users': users_online})


@main.route('/private_chat/<send_to>')
@login_required
def join_private_chat(send_to):
    """View for private send private message to a user"""
    session['send_to'] = send_to
    session['sender'] = current_user.username
    return redirect(url_for('.private_chat'))


@main.route('/private_chat')
@login_required
def private_chat():
    send_to = session.get('send_to')
    # sender = session.get('sender')
    return render_template('private_chat.html', send_to=send_to)


@main.route('/create_channel', methods=['POST', 'GET'])
@login_required
def create_channel():
    """ Register a user """
    # Form is filled then we send it via POST
    if request.method == 'POST':
        name = request.form.get('name')
        description = request.form.get('description')

        channel = Channel.query.filter_by(name=name).first()
        # If a channel is found, redirect back to create page so user can try again
        if channel:
            flash('Channel already exists')
            return redirect(url_for('.create_channel'))

        # Create new channel with the form data.
        new_channel = Channel(name=name, description=description)

        # add the new channel to the database
        db.session.add(new_channel)
        db.session.commit()

        return redirect(url_for('.index'))

    # Show the create channel page
    return render_template('create_channel.html')
