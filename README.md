**Flask-SocketIO-Chat**
=======================

A simple chat application that demonstrates how to structure a Flask-SocketIO application. This guide aims to explain how to run the application locally (on your machine), and how to use it. For the online version, please refer to the deployment branch. Who I point out is still under development.


**Prerequisites & How to install**
----------------------------------

### 1. Prerequisites

This app was developed using a virtual environment.You must therefore have a virtual environment manager for python such as [pipenv](https://pipenv-fork.readthedocs.io/en/latest/), [virtualenv](https://docs.python.org/fr/3/library/venv.html) or others. Here I used virtualenv.
The python version used is *python 3.7.3*.

### 2. How to install

- First clone the project repository. In your terminal:
`git clone https://Vector22@bitbucket.org/Vector22/my-chat.git`

- Go in the root folder of the app:
`cd my-chat`

- Create the virtual environment(acording to virtualenv):
`virtualenv -p python3 env`
The line above create a virtual environment named env with *python3* as python version.

- Activate it by typing:
`source /env/bin/activate`

- Now install the requirements of the app:
`pip install -r requirements.txt`
After a couples of minutes, it would be done.

- After that, you would be able to initialize the database used by the app.
Since we are on development, we use sqlite3 database because it's easy to install an use for our app. For production we'll use a more robust database system like postgresql. Run in your terminal:
`export FLASK_APP=chat.py`
`flask db upgrade`
The first one is to tell flask to use *chat.py* as entrypoint of the application.
And the second create the database shemas of the app.

- Run the app
`flask run`
Go to [http://localhost:5000] to see the app main page


**How to use the app**
----------------------

### 1. Create an acccount

When you lunch the app for the first time, you'll se a login page.
Try first to create an account by following the **signup** link in the navbar.
After login with your credentials you used to create the account.

### 2. Join a channel

On your left , you are a list of some predefined (dummy) channels that you can join
by clicking on the box.
To simulate a real situation, you must have at least two accounts and join the same channel in two different browser windows. Then try to write something and validate with enter.
There you have it !

### 3. Engage in private conversation

Here too it is very simple. On the right, we have the list of users connected to the site. To send a private message to another user, click on his box. on the other side, you will have to click on the box of the corresponding user. And here you are ready to send you private messages without polluting the main channels...

### 4. Leave a conversation

To leave a conversation, you must click on the bottom link of the chat page named often __leave this conversation__ or __leave this channel__.
You will be redirect bach to the home page.

### 5. Create a custom channel

To do that, simply click on the bottom left of the home page, the button named **create a new channel**.
Enter a name and description, save it and voila.

### 6. Delete a channel

__Coming soon__


**Difficulties encountered with improvements**
----------------------------------------------


